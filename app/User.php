<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'avatar', 'bio', 'role','serial_no',
        'device_id','device_token','device_type','login_status','isdelete',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];



    public static function onlineuser()
    {
       return User::where('role','!=',10)->where('isdelete','0')->where('login_status',1)->get();
    }
    
    
    public static function activeuser()
    {
       return User::where('role','!=',10)->where('isdelete','0')->get();
    }
    
    public static function offlineuser()
    {
       return User::where('role','!=',10)->where('isdelete','0')->where('login_status',0)->get();
    }
    
    public static function deleteuser()
    {
       return User::where('role','!=',10)->where('isdelete','1')->get();
    }
    /*
    |------------------------------------------------------------------------------------
    | Api Login
    |------------------------------------------------------------------------------------
    */

    public static function login($request){
        return User::all();
        
    }



    /*
    |------------------------------------------------------------------------------------
    | Validations
    |------------------------------------------------------------------------------------
    */
    public static function rules($update = false, $id = null,$request = null)
    {
        if ($update && $id) {
                if($request)
                { 
                    $dbserial_no =   User::where('id','=',$id)->select('serial_no')->first();   
                        if($dbserial_no['serial_no'] == $request->input('serial_no')){
                                   
                            return $common = ['avatar' => 'image',];
                            }
                            else{
                                
                              return  $common = [                               
                                    'serial_no' => 'unique:users',
                                    'avatar' => 'image',];
                                }
                }
                else
                {
                     $common = ['avatar' => 'image',];
                }
            return $common;
        }
        else
        {
            $common = [
            'email'    => "required|email|unique:users,email,$id",
            'password' => 'nullable|confirmed',
            'serial_no' => 'unique:users',
            'avatar' => 'image',
        ];
        }

        return array_merge($common, [
            'email'    => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /*
    |------------------------------------------------------------------------------------
    | Attributes
    |------------------------------------------------------------------------------------
    */
    public function setPasswordAttribute($value='')
    {
        $this->attributes['password'] = bcrypt($value);
    }
    
    public function getAvatarAttribute($value)
    {
        if (!$value) {
            return 'http://placehold.it/160x160';
        }
    
        return config('variables.avatar.public').$value;
    }
    public function setAvatarAttribute($photo)
    {
        $this->attributes['avatar'] = move_file($photo, 'avatar');
    }

    /*
    |------------------------------------------------------------------------------------
    | Boot
    |------------------------------------------------------------------------------------
    */
    public static function boot()
    {
        parent::boot();
        static::updating(function ($user) {
            $original = $user->getOriginal();
            
            if (\Hash::check('', $user->password)) {
                $user->attributes['password'] = $original['password'];
            }
        });
    }



    
}
