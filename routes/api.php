<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
}); */

Route::apiResource('/user', 'ApisController'); 

/* AUTHENTICATION API'S */

Route::post('/user/login','ApisController@login');

Route::post('/user/updatetoken','ApisController@updatetoken');

Route::post('/user/markattendance','ApisController@markattendance');

Route::post('/user/getuserattendance','ApisController@getuserattendance');

Route::post('/user/addattendanceimages','ApisController@addattendanceimages');

Route::post('/user/getmonthlyattendance','ApisController@getmonthlyattendance');

Route::post('/user/getmonthlyattendancepictures','ApisController@getmonthlyattendancepictures');

Route::post('/user/logout','ApisController@logout');
